# Installing Kubernates:
# Can follow: https://vitux.com/install-and-deploy-kubernetes-on-ubuntu/
# Important Check:
# Unique hostname, MAC Address product_uuid for every node
# Make sure ports are open: See https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/install-kubeadm/#check-required-ports
# Check product uuid sudo cat /sys/class/dmi/id/product_uuid
lsmod | grep br_netfilter
# Make sure that the br_netfilter module is loaded
# to load explicitlly
modprobe br_netfilter
# Make sure net.bridge.bridge-nf-call-iptables =  in sysctl config
sudo sh -c "cat <<EOF > /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
EOF"
sudo sysctl --system


# Disable swap memory
sudo swapoff -a

# Install kubectl
# 1. Download Latest release
sudo apt-get update && sudo apt-get install -y apt-transport-https curl
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
cat <<EOF | sudo tee /etc/apt/sources.list.d/kubernetes.list
deb https://apt.kubernetes.io/ kubernetes-xenial main
EOF
sudo apt-get update
sudo apt-get install -y kubeadm
sudo apt-mark hold kubeadm
kubeadm version

# Join master node
sudo kubeadm join 172.31.35.69:6443 --token kri3ro.kog7yjkosdukkdav \
    --discovery-token-ca-cert-hash sha256:e6f10f1218243f228fbf5aed0448fa55837fa427ca4c41023071ab6ed4135b97
