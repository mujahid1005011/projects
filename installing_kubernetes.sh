# Installing Kubernates:
# Can follow: https://vitux.com/install-and-deploy-kubernetes-on-ubuntu/
# Important Check:
# Unique hostname, MAC Address product_uuid for every node
# Make sure ports are open: See https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/install-kubeadm/#check-required-ports
# Check product uuid sudo cat /sys/class/dmi/id/product_uuid
lsmod | grep br_netfilter
# Make sure that the br_netfilter module is loaded
# to load explicitlly
modprobe br_netfilter
# Make sure net.bridge.bridge-nf-call-iptables =  in sysctl config
sudo sh -c "cat <<EOF > /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
EOF"
sudo sysctl --system


# Disable swap memory
sudo swapoff -a

# Install kubectl
# 1. Download Latest release
sudo apt-get update && sudo apt-get install -y apt-transport-https curl
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
cat <<EOF | sudo tee /etc/apt/sources.list.d/kubernetes.list
deb https://apt.kubernetes.io/ kubernetes-xenial main
EOF
sudo apt-get update
sudo apt-get install -y kubelet kubeadm kubectl
sudo apt-mark hold kubelet kubeadm kubectl
kubectl version --client

sudo systemctl daemon-reload
sudo systemctl restart kubelet

sudo kubeadm init --pod-network-cidr=10.244.0.0/16

mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config


# Deploy pod network
sudo kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml